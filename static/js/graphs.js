queue()
    .defer(d3.json, "../static/data/pimco_data.csv")
    .defer(d3.json, "../static/geojson/us-states.json")
    .await(makeGraphs);

function makeGraphs(error, projectsJson, statesJson) {
	
	//Clean projectsJson data
	var donorschooseProjects = projectsJson;
	var dateFormat = d3.time.format("%d/%m/%y");
	donorschooseProjects.forEach(function(d) {
		d["trade_date"] = dateFormat.parse(d["trade_date"]);
		d["trade_date"].setDate(1);
		d["net_flow"] += d["net_flow"];
		d["sales"] += d["sales"];
		d["redemptions"] += d["redemptions"];
		d["income_reinvest"] += d["income_reinvest"];

	});

	//Create a Crossfilter instance
	var ndx = crossfilter(donorschooseProjects);

	//Define Dimensions
	var dateDim = ndx.dimension(function(d) { return d["trade_date"]; });
	var sectorDim = ndx.dimension(function(d) { return d["sector"]; });
	var stateDim = ndx.dimension(function(d) { return d["state_code"]; });
	var netFlowDim  = ndx.dimension(function(d) { return d["net_flow"]; });

	//Calculate metrics
	var netFlowByDate = dateDim.group().reduceSum(function(d) {
		return d["net_flow"];
	});
	var netFlowBySector = sectorDim.group().reduceSum(function(d) {
		return d["net_flow"];
	});
	var netFlowByState = stateDim.group().reduceSum(function(d) {
		return d["net_flow"];
	});

	var all = ndx.groupAll();
	var totalredemptions = ndx.groupAll().reduceSum(function(d) {return d["redemptions"];});

	var all = ndx.groupAll();
	var totalsales = ndx.groupAll().reduceSum(function(d) {return d["sales"];});

	var all = ndx.groupAll();
	var total_income_reinvest = ndx.groupAll().reduceSum(function(d) {return d["income_reinvest"];});


	var max_state = netFlowByState.top(1)[0].value;
	var min_state = -100000;

	//Define values (to be used in charts)
	var minDate = dateDim.bottom(1)[0]["trade_date"];
	var maxDate = dateDim.top(1)[0]["trade_date"];

    //Charts
	var timeChart = dc.lineChart("#time-chart");
	var resourceTypeChart = dc.rowChart("#resource-type-row-chart");
	var usChart = dc.geoChoroplethChart("#us-chart");

	var totalRedemptionsND = dc.numberDisplay("#total-redemptions-nd");
	var totalSalesND = dc.numberDisplay("#total-sales-nd");
	var totalReinvestND = dc.numberDisplay("#total-reinvest-nd");

	totalRedemptionsND
		.formatNumber(d3.format("d"))
		.valueAccessor(function(d){return d; })
		.group(totalredemptions)
		.formatNumber(d3.format(".4s"));

	totalSalesND
		.formatNumber(d3.format("d"))
		.valueAccessor(function(d){return d; })
		.group(totalsales)
		.formatNumber(d3.format(".4s"));

	totalReinvestND
		.formatNumber(d3.format("d"))
		.valueAccessor(function(d){return d; })
		.group(total_income_reinvest)
		.formatNumber(d3.format(".4s"));

	timeChart
		.width(800)
		.height(320)
		.margins({top: 10, right: 10, bottom: 30, left: 100})
		.dimension(dateDim)
		.group(netFlowByDate)
		.transitionDuration(500)
		.x(d3.time.scale().domain([new Date(2015, 02, 01), new Date(2015, 06, 01)],1))
		.elasticY(true)
		.xAxisLabel("Year")
		.yAxis().ticks(4)
		;

	resourceTypeChart
        .width(800)
        .height(600)
        .dimension(sectorDim)
        .group(netFlowBySector)
        .xAxis().ticks(6);


	usChart.width(900)
		.height(600)
		.dimension(stateDim)
		.group(netFlowByState)
		.colors(d3.scale.quantize().range(["#e52631", "#C4E4FF", "#9ED2FF", "#81C5FF", "#6BBAFF", "#51AEFF", "#36A2FF", "#1E96FF", "#0089FF", "#009933"]))
		.colorDomain([min_state, max_state])
		.colorCalculator(function (d) { return d ? usChart.colors()(d) : '#ccc'; })
		.overlayGeoJson(statesJson["features"], "state", function (d) {
			return d.properties.name;
		})
		.projection(d3.geo.albersUsa()
    				.scale(800)
    				.translate([450, 350]))
		.title(function (p) {
			return "State: " + p["key"]
					+ "\n"
					+ "Net Flow: $" + Math.round(p["value"]) ;
		})

    dc.renderAll();

};