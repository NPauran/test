import os
from flask import Flask, render_template_string, render_template
from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy
from flask_user import login_required, UserManager, UserMixin, SQLAlchemyAdapter
from flask.ext.user.forms import RegisterForm
from wtforms import BooleanField, HiddenField, PasswordField, SubmitField, StringField
from flask.ext.login import current_user
from flask.ext.cache import Cache

import pandas as pd
import psycopg2 as pg

app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Test passed'

if __name__ == '__main__':
    app.run()