#more Nargis comments
import os
from flask import Flask, render_template_string, render_template
from flask_mail import Mail
from flask_sqlalchemy import SQLAlchemy
from flask_user import login_required, UserManager, UserMixin, SQLAlchemyAdapter
# from flask.ext.user.forms import RegisterForm
from flask.ext.user.forms import RegisterForm
from wtforms.fields import BooleanField, HiddenField, PasswordField, SubmitField, StringField, SelectField
#from wtforms import BooleanField, HiddenField, PasswordField, SubmitField, StringField, SelectField
from flask.ext.login import current_user
from flask.ext.cache import Cache
from fundChoices import fundChoices

import pandas as pd
import psycopg2 as pg

#Global vars
config = {}
FUND_FAMILY = 'All'
FundFamilyNames =[]

#Loads configuration file
def load_config():
	for l in open('config.txt','r'):
		if l.startswith('#'):
			continue
		else:
			al = l.split('=')
			config[al[0].strip()] = al[1].strip()
	return 0


def load_fund_families():
    """load all the names of the fund families straight from db
    :returns: TODO

    """
    connection_string = "host='%s' port=%s user='%s' password='%s' dbname='%s'" %(config['host'],config['port'],config['user'],config['password'],config['dbname'])
    connection = pg.connect(connection_string)
    cur = connection.cursor()
    sql = """ SELECT distinct fundfamilyname,  fundfamilyname
    FROM jameslondal.ff_myflows
    where fundfamilyname is not null
    limit 100;"""
    cur.execute(sql)
    global FundFamilyNames
    FundFamilyNames = cur.fetchall()
    return 0


# Use a Class-based config to avoid needing a 2nd file
# os.getenv() enables configuration through OS environment variables
class ConfigClass(object):

	# configuration
	DEBUG = True
	SECRET_KEY = 'hidden'
	USERNAME = 'secret'
	PASSWORD = 'secret'
	SQLALCHEMY_DATABASE_URI = 'sqlite:///basic_app.sqlite'
	CSRF_ENABLED = True # otherwise forms not working. google it

	# Flask-Mail settings
	MAIL_SERVER='smtp.gmail.com'
	MAIL_PORT = 465
	MAIL_USE_TLS = False
	MAIL_USE_SSL= True
	MAIL_DEFAULT_SENDER = '"Fund Flows" <flaskfundflows123@gmail.com>'
	MAIL_USERNAME = 'flaskfundflows123@gmail.com'
	MAIL_PASSWORD ='James0071!'

	# Flask-User settings
	USER_APP_NAME		= "DST Fund Flows"				# Used by email templates
	USER_ENABLE_CHANGE_USERNAME	= False


	USER_ENABLE_CONFIRM_EMAIL	  = False
	USER_ENABLE_LOGIN_WITHOUT_CONFIRM = True
	
def create_app():
	""" Flask application factory """

	# Setup Flask app and app.config
	app = Flask(__name__)
	app.config.from_object(__name__+'.ConfigClass')

	# Initialize Flask extensions
	db = SQLAlchemy(app)							# Initialize Flask-SQLAlchemy
	mail = Mail(app)								# Initialize Flask-Mail

	# Define the User data model. Make sure to add flask.ext.user UserMixin !!!
	class User(db.Model, UserMixin):
		id = db.Column(db.Integer, primary_key=True)

		# User authentication information
		username = db.Column(db.String(50), nullable=False, unique=True)
		password = db.Column(db.String(255), nullable=False, server_default='')
		reset_password_token = db.Column(db.String(100), nullable=False, server_default='')

		fund_family = db.Column(db.String(50), nullable=False, default='')

		# User email information
		email = db.Column(db.String(255), nullable=False, unique=True)
		confirmed_at = db.Column(db.DateTime())

		# User information
		active = db.Column('is_active', db.Boolean(), nullable=False, server_default='0')
		first_name = db.Column(db.String(100), nullable=False, server_default='')
		last_name = db.Column(db.String(100), nullable=False, server_default='')

	# Create all database tables
	db.create_all()

	class MyRegisterForm(RegisterForm):
		load_config() # config is not available otherwise although a global variable
		load_fund_families() # r
		#fund_family = StringField('Fund Family')
		fund_family = SelectField(u'Fund Family',
                            choices= FundFamilyNames
                            )


	# Setup Flask-User
	db_adapter = SQLAlchemyAdapter(db, User)		# Register the User model
	user_manager = UserManager(db_adapter, app, register_form=MyRegisterForm)	# Initialize Flask-User

	return app

#Setup server
app = create_app()
cache = Cache(app,config={'CACHE_TYPE': 'simple'})
cache.init_app(app, config={'CACHE_TYPE': 'simple'})


# MyFlows reports
@app.route('/members/myflows')
@login_required                                 # Use of @login_required decorator
def regional():
    return render_template('myflows.html')

@app.route('/members/industry_flows')
@login_required                                 # Use of @login_required decorator
def benchmark():
    return render_template('comp.html')

@app.route('/')
def industry_summary_dashboard():
    return render_template('industry_summary.html')

#test page
@app.route('/members/private_summary')
@login_required
def local_summary_dashboard():
	return render_template('local_summary.html')

#test page
@app.route('/members/recommendation_summary')
@login_required
def recommendation_summary_dashboard():
	return render_template('recommendation_summary.html')

#Get Data API
@app.route('/api/<report>/<username>')
@login_required
@cache.cached(timeout=12000)
def get_report_data(report,username):
	connection_string = "host='%s' port=%s user='%s' password='%s' dbname='%s'" 
						% (config['host'],
						config['port'],config['user'],
						config['password'],
						config['dbname'])
	connection = pg.connect(connection_string)
	cur = connection.cursor()
	if report == 'market_flows':
		sql = """

			select
			trade_date,
			social_code,
			sum(sales::float)::text as sales,
			sum(net_flow::float)::text as net_flow
			from
			jameslondal.ff_social_code_market_flows
			group by
			trade_date,
			social_code
			order by
			social_code,
			trade_date::date
			"""

		cur.execute(sql)
		
		rows = cur.fetchall()
		out = "trade_date,social_code,sales,net_flows\n"
		for row in rows:
			out = out + ",".join(row) + "\n"
	

	elif report == 'industry_summary':
		sql = """
			select
			 fundfamilyname
			,trade_date::text as trade_date
			,ratingoverall::text as ratingoverall
			,shareclasstype
			,social_code
			,gross_sales::text as gross_sales
			,redemptions::text as redemptions
			,net_sales::text as net_sales
			from
			jameslondal.ff_summary_fund_family_public
			order by
			fundfamilyname
			 fundfamilyname
			,shareclasstype
			,social_code
			,trade_date
			"""

		cur.execute(sql)

		rows = cur.fetchall()
		
		out = "fundfamilyname,trade_date,ratingoverall,shareclasstype,social_code,gross_sales,redemptions,net_sales\n"
		for row in rows:
			out = out + ",".join(row) + "\n"
	
	elif report == 'local_summary':
		sql = """
			select
			 investmentvehiclename
			,trade_date::text as trade_date
			,ratingoverall::text as ratingoverall
			,shareclasstype
			,social_code
			,gross_sales::text as gross_sales
			,redemptions::text as redemptions
			,net_sales::text as net_sales
			from
			jameslondal.ff_summary_fund_family
			where
			 fundfamilyname = '%s'
			order by
			 investmentvehiclename
			,ratingoverall
			,shareclasstype
			,social_code
			,trade_date
			""" % current_user.fund_family

		cur.execute(sql)
		
		rows = cur.fetchall()
		out = "investmentvehiclename,trade_date,ratingoverall,shareclasstype,social_code,gross_sales,redemptions,net_sales\n"
		for row in rows:
			out = out + ",".join(row) + "\n"

	
	elif report == 'recommendation_summary':
		sql = """
			select
			shareclasstype,
			social_code
			from
			jameslondal.ff_summary_fund_family
			where
			 fundfamilyname = '%s'
			""" % current_user.fund_family

		cur.execute(sql)
		
		rows = cur.fetchall()
		out = "shareclasstype,social_code\n"
		for row in rows:
			out = out + ",".join(row) + "\n"

		
	elif report == 'myflows':
		sql = """
				select
				sector,
				state,
				trade_date,
				gross_sales,
				redemptions,
				net_sales,
				income_reinvest
				from
				jameslondal.ff_region
				where
				fundfamilyname = '%s'
				and trade_date > '2016-02-28 00:00:00+01'
				and state in ('London', 'Surrey')
				and income_reinvest > '0' and net_sales > '1' and gross_sales > '1'
			""" % current_user.fund_family

		cur.execute(sql)

		rows = cur.fetchall()
		out = "sector,state,trade_date,sales,redemptions,net_flow,income_reinvest\n"
		for row in rows:
			out = out + ",".join(row) + "\n"

	elif report == 'rest_of_market':
		sql = """
				select
				sector,
				state,
				trade_date,
				gross_sales,
				redemptions,
				net_sales,
				income_reinvest
				from
				jameslondal.ff_region
			where
			fundfamilyname <> '%s' and
			sector in (
							select
							distinct
							sector
							from
							jameslondal.ff_region
							where
							fundfamilyname = '%s'
			)
		""" % (current_user.fund_family,current_user.fund_family)

		cur.execute(sql)

		rows = cur.fetchall()
		out = "sector,state,trade_date,sales,redemptions,net_flow,income_reinvest\n"
		for row in rows:
			out = out + ",".join(row) + "\n"

	return out

# Start development web server
if __name__=='__main__':
    load_config()
    print(config)
    app.run(host=config['hostname'], port=80)

#comment
####
