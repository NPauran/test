drop table if exists jameslondal.ff_holdings;
create table  jameslondal.ff_holdings (
hld_id bigint,
office_id text,
firm_name text,
sub_accounting text,
rep_id text,
account text,
mutual_fund_lng_nm text,
cusip text,
social_code text,
globalcategoryname text,
shareclasstype text,
fundid bigint,
investmentvehiclename text,
fundfamilyname text,
actual12b1 text,
trailingreturnytd text,
trailingreturny1 text,
annualreportnetexpenseratio numeric,
ratingoverall numeric,
ristratingoverall numeric,
city text,
state text,
brch_zip_cde text,
aum numeric
);


insert into jameslondal.ff_holdings 
select ordernumber as hld_id ,
division as office_id,
colour as firm_name,
size as sub_accounting,
productnumber as rep_id,
brandname as account,
producttype as mutual_fund_lng_nm,
orderlinenumber as cusip,
value_segment as social_code,
producttype as globalcategoryname,
gender as shareclasstype,
t2.customernumber as fundid,
firstname as investmentvehiclename,
division as fundfamilyname,
title as actual12b1,
address1 as trailingreturnytd,
address2 as trailingreturny1,
nodm as annualreportnetexpenseratio,
noemail as ratingoverall,
noemail as ristratingoverall,
city,
county as state,
postcode as brch_zip_cde,
listprice as aum
from
liberty_data_science.orders t1
join 
liberty_data_science.customer_full_platinum t2
on 
t1.customernumber = t2.customernumber
where orderdate > '2015-01-01'



drop table if exists jameslondal.ff_transactions;
create table  jameslondal.ff_transactions(
 trade_date date, 
 hld_id bigint,
 gross_sales numeric,
 redemptions numeric,
 net_sales numeric,
 income_reinvest numeric,
 income_out numeric,
 office_id text,
 sub_accounting text,
 rep_id text,
 account text,
 state_code text,
 cusip text,
 social_code text,
 globalcategoryname text,
 shareclasstype text,
 fundid text,
 investmentvehiclename text,
 fundfamilyname text,
 actual12b1 text,
 trailingreturnytd text,
 trailingreturny1 text,
 annualreportnetexpenseratio numeric,
 ratingoverall numeric,
 riskratingoverall numeric,
 brch_zip_cde text,
 firm_name text,
 city text,
 state text);


insert into jameslondal.ff_transactions
select orderdate as trade_date,
ordernumber as hld_id ,
salerevenue - discount as gross_sales,
(salerevenue - salerevenue/4) - listprice as redemptions,
(salerevenue - discount) - ((salerevenue - salerevenue/4)- listprice) as net_sales,
discount as income_reinvest,
listprice as income_out,
division as office_id,
size as sub_accounting,
productnumber as rep_id,
brandname as account,
postcode as state_code,
orderlinenumber as cusip,
value_segment as social_code,
producttype as globalcategoryname,
gender as shareclasstype,
t2.customernumber as fundid,
firstname as investmentvehiclename,
division as fundfamilyname,
title as actual12b1,
address1 as trailingreturnytd,
address2 as trailingreturny1,
nodm as annualreportnetexpenseratio,
donotcall as ratingoverall,
noemail as ristratingoverall,
postcode as brch_zip_cde,
colour as firm_name,
city,
county as state
from
liberty_data_science.orders t1
join 
liberty_data_science.customer_full_platinum t2
on 
t1.customernumber = t2.customernumber
where orderdate > '2015-01-01'



ALTER TABLE jameslondal.ff_holdings
	ALTER COLUMN ratingoverall TYPE numeric
		USING CASE 
		when fundfamilyname = 'BEAUTY' then 2
		when fundfamilyname = 'ACCESSORIES' then 4
		when fundfamilyname = 'LADIES' then 3
		when fundfamilyname = 'CHILDREN' then 5
		when fundfamilyname = 'HOME' then 1
		else 6 
		END;

ALTER TABLE jameslondal.ff_transactions
	ALTER COLUMN ratingoverall TYPE numeric
		USING CASE 
		when fundfamilyname = 'BEAUTY' then 2
		when fundfamilyname = 'ACCESSORIES' then 4
		when fundfamilyname = 'LADIES' then 3
		when fundfamilyname = 'CHILDREN' then 5
		when fundfamilyname = 'HOME' then 1
		else 6 
		END;

ALTER TABLE jameslondal.ff_transactions
	ALTER COLUMN shareclasstype TYPE text
	using case 
	when shareclasstype='F' then 'Classic'
	when shareclasstype='M' then 'Modern'
	else 'Urban'
	end 

ALTER TABLE jameslondal.ff_holdings
	ALTER COLUMN shareclasstype TYPE text
	using case 
	when shareclasstype='F' then 'Classic'
	when shareclasstype='M' then 'Modern'
	else 'Urban'
	end 

drop table if exists jameslondal.ff_summary_fund_family_public;
create table jameslondal.ff_summary_fund_family_public as (
select
globalcategoryname,
trade_date,
ratingoverall,
shareclasstype,
social_code,
sum(gross_sales) as gross_sales,
sum(redemptions) as redemptions,
sum(net_sales) as net_sales
from
jameslondal.ff_transactions t1
group by
trade_date,
globalcategoryname,
ratingoverall,
shareclasstype,
social_code
);
drop table if exists jameslondal.ff_summary_fund_family;
create table jameslondal.ff_summary_fund_family as (
select
fundfamilyname,
trade_date,
investmentvehiclename,
ratingoverall,
shareclasstype,
social_code,
sum(gross_sales) as gross_sales,
sum(redemptions) as redemptions,
sum(net_sales) as net_sales
from
jameslondal.ff_transactions t1
group by
trade_date,
fundfamilyname,
investmentvehiclename,
ratingoverall,
shareclasstype,
social_code 
);

drop table if exists jameslondal.ff_region;
create table jameslondal.ff_region as (
select
fundfamilyname,
globalcategoryname as sector,
state,
date_trunc('month',trade_date)::text as trade_date,
coalesce(sum(gross_sales),0)::text as gross_sales,
coalesce(sum(redemptions),0)::text as redemptions,
coalesce(sum(net_sales ),0)::text as net_sales,
coalesce(sum(income_reinvest),0)::text as income_reinvest
from
jameslondal.ff_transactions
group by
fundfamilyname,
globalcategoryname,
state,
date_trunc('month',trade_date)::text
)
;

drop table if exists jameslondal.ff_state_sector_aum;
create table  jameslondal.ff_state_sector_aum as (
select
globalcategoryname,
state,
sum(aum)as aum,
social_code
from
jameslondal.ff_holdings 
group by
globalcategoryname,
state,
social_code
order by
sum(aum) desc 
);


drop table if exists jameslondal.ff_myflows;
create table jameslondal.ff_myflows as (
select
fundfamilyname,
globalcategoryname as sector,
state,
date_trunc('month',trade_date)::text as trade_date,
coalesce(sum(gross_sales),0)::text as gross_sales,
coalesce(sum(redemptions),0)::text as redemptions,
coalesce(sum(net_sales ),0)::text as net_sales,
coalesce(sum(income_reinvest),0)::text as income_reinvest
from
jameslondal.ff_transactions 
group by
fundfamilyname,
globalcategoryname,
state,
date_trunc('month',trade_date)::text
);



drop table if exists jameslondal.ff_preformance;
create table jameslondal.ff_preformance as (
select
fundfamilyname,
globalcategoryname,
date_trunc('month',trade_date)::text as trade_date,
coalesce(sum(gross_sales),0)::text as sales,
coalesce(sum(redemptions),0)::text as redemptions,
coalesce(sum(net_sales),0)::text as net_flow,
coalesce(sum(income_reinvest),0)::text as income_reinvest
from
jameslondal.ff_transactions t1 
group by
fundfamilyname,
globalcategoryname,
date_trunc('month',trade_date)::text
);


drop table if exists jameslondal.ff_social_code_flows;
create table jameslondal.ff_social_code_flows as (
select
fundfamilyname,
investmentvehiclename,
date_trunc('month',trade_date)::text as trade_date,
social_code,
coalesce(sum(gross_sales),0)::text as sales,
coalesce(sum(redemptions),0)::text as redemptions,
coalesce(sum(net_sales),0)::text as net_flow,
coalesce(sum(income_reinvest),0)::text as income_reinvest
from
jameslondal.ff_transactions 
group by
fundfamilyname,
investmentvehiclename,
date_trunc('month',trade_date)::text,
social_code
);


drop table if exists jameslondal.ff_master_dashboard2;
create table jameslondal.ff_master_dashboard2 as (
select
state,
globalcategoryname as sector,
date_trunc('month',trade_date)::date::text as trade_date,
social_code,
coalesce(sum(gross_sales),0)::text as sales,
coalesce(sum(redemptions),0)::text as redemptions,
coalesce(sum(net_sales),0)::text as net_flow,
coalesce(sum(income_reinvest),0)::text as income_reinvest
from
jameslondal.ff_transactions 
group by
state,
globalcategoryname ,
date_trunc('month',trade_date)::date::text ,
social_code
);


drop table if exists jameslondal.ff_social_code_market_flows;
create table jameslondal.ff_social_code_market_flows as (
select
fundfamilyname,
trade_date,
social_code,
coalesce(sum(sales::float),0)::text as sales,
coalesce(sum(redemptions::float),0)::text as redemptions,
coalesce(sum(net_flow::float),0)::text as net_flow,
coalesce(sum(income_reinvest::float),0)::text as income_reinvest
from
jameslondal.ff_social_code_flows_sector t1
group by
fundfamilyname,
trade_date,
social_code 
order by
fundfamilyname,
social_code,
trade_date
);



drop table if exists jameslondal.ff_social_code_flows_sector;
create table jameslondal.ff_social_code_flows_sector as (
select
fundfamilyname,
globalcategoryname as sector,
date_trunc('month',trade_date)::text as trade_date,
social_code,
coalesce(sum(gross_sales),0)::text as sales,
coalesce(sum(redemptions),0)::text as redemptions,
coalesce(sum(net_sales),0)::text as net_flow,
coalesce(sum(income_reinvest),0)::text as income_reinvest
from
jameslondal.ff_transactions 
group by
fundfamilyname,
globalcategoryname,
date_trunc('month',trade_date)::text,
social_code
);
