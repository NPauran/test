﻿
----------------------- Social Code Mappings by Account A87-------------------------
set search_path to assetmgt;

drop table if exists tmp1;
create temp table tmp1 as (
select
md5(a87.system_id || a87.account || j72.fund_code ) as hld_id, 
a87.system_id,
a87.repnumber,
a87.account,
a87.sahouse,
a87.resstatecountry,
a87.totshrsiss,
a87.totshrsuiss,
j72.mgmt_code,
j72.fund_sponsor_id,
j72.mutual_fund_lng_nm,
j72.master_mf_id,
j72.fund_code,
j72.sec_iss_id,
j72.ticker_symbol_id,
a87.socialcode as ta_socialcode,
j68.fund_sponsor_nme,
case when fs_opr_mode_cd = 'M' then 1 else 0 end sub_accounting,
dealer,
dealerbrch,
case 
 when dealercntl = 0 then 'Non-Network' 
 when dealercntl = 1 then 'Broker Margin IRA'
 when dealercntl = 2 then 'Cust Name Broker'
 when dealercntl = 3 then 'Brokerage Broker'
 when dealercntl = 4 then 'Brokerage Fund'
end as access_level,
count(*) as count
from
a87 join 
j72 on (a87.fund = j72.fund_code and a87.system_id = j72.system_id) join 
j68 on (j72.fund_sponsor_id = j68.fund_sponsor_id and j72.system_id = j68.system_id) 
where
a87.openclose = 'O'
group by
md5(a87.system_id || a87.account || j72.fund_code ), 
a87.system_id,
a87.repnumber,
a87.account,
a87.sahouse,
a87.resstatecountry,
a87.totshrsiss,
a87.totshrsuiss,
j72.mgmt_code,
j72.fund_sponsor_id,
j72.mutual_fund_lng_nm,
j72.master_mf_id,
j72.fund_code,
j72.sec_iss_id,
j72.ticker_symbol_id,
a87.socialcode,
j68.fund_sponsor_nme,
case when fs_opr_mode_cd = 'M' then 1 else 0 end,
dealer,
dealerbrch,
case 
 when dealercntl = 0 then 'Non-Network' 
 when dealercntl = 1 then 'Broker Margin IRA'
 when dealercntl = 2 then 'Cust Name Broker'
 when dealercntl = 3 then 'Brokerage Broker'
 when dealercntl = 4 then 'Brokerage Fund'
end
order by count(*) desc
);

--125738158 
drop table if exists tmp2;
create temp table  tmp2 as
select
t1.*,
case 
  when oh6.nscc_social_code = '02' then '01' -- old women social code that no longer exists
  when oh6.nscc_social_code = '10' then '34' -- no longer exists
  else oh6.nscc_social_code
end as nscc_social_code
from
tmp1 t1 left join
oh6 on (
t1.system_id = oh6.system_id and
t1.ta_socialcode = oh6.social_code and
t1.mgmt_code = oh6.mgmt_code and
nscc_default_cd = 'Y'
)
DISTRIBUTED BY (system_id)

;

--select count(*) from tmp3
--125738158  

drop table if exists tmp3;
create temp table  tmp3 as (
with oh5_l as (
select
distinct 
system_id,
dflt_social_code,
nscc_default_cd,
nscc_social_code
from
oh5
where
nscc_default_cd = 'Y'
)
select
t1.*,
case 
  when oh5_l.nscc_social_code = '02' then '01' -- old women social code that no longer exists
  when oh5_l.nscc_social_code = '10' then '34' -- no longer exists
  else oh5_l.nscc_social_code
end as nscc_social_code_oh5
from
tmp2 t1 left join
oh5_l on (
t1.system_id = oh5_l.system_id and
t1.ta_socialcode = oh5_l.dflt_social_code 
)
) 
DISTRIBUTED BY (system_id)
;

--select count(*) from tmp4
--129163940 

drop table if exists tmp4;
create temp table  tmp4 as (
with bn2_l as (
select distinct
system_id,
social_code,
fund_sponsor_id,
agent_id,
plan_type_cde,
social_cd_house_acct_cd
from
bn2
)
select
t1.*,
agent_id,
plan_type_cde,
social_cd_house_acct_cd
from
tmp3 t1 left join
bn2_l on (
t1.system_id = bn2_l.system_id and
t1.ta_socialcode = bn2_l.social_code and
t1.fund_sponsor_id = bn2_l.fund_sponsor_id
)
)
DISTRIBUTED BY (system_id)
;

--select count(*) from tmp6
--129163940 

drop table if exists tmp6;
create temp table  tmp6 as (
select
t1.*,
d.fund_grp_number
from
tmp4 t1 left join
 (select 
 distinct 
  a.system_id, 
  a.fund_grp_number, 
  a.fund_code 
 from d60 a inner join 
 assetmgt.d61 b on a.fund_grp_number = b.fund_grp_number and b.fund_grp_class_cde = 4) d on 
   (
    t1.system_id = d.system_id and
    t1.fund_code = d.fund_code
    )
)
DISTRIBUTED BY (system_id)   
;

--select count(*) from tmp7
--129163940 

drop table if exists tmp7;
create temp table  tmp7 as (
select
t1.*,
g.fincl_inst_name_1 as firm_name
from
tmp6 t1 left join
 (select distinct system_id, fi_fund_group_nbr, financial_inst_id, fincl_inst_name_1 from g02 where financial_inst_id is not null) g on 
   (
    t1.system_id = g.system_id and
    t1.fund_grp_number = g.fi_fund_group_nbr and
    t1.dealer = g.financial_inst_id
    )
) 
DISTRIBUTED BY (system_id)

;

--select count(*) from tmp8
--129163940 

drop table if exists tmp8;
create temp table tmp8 as (
select
t1.*,
g.fi_brch_zip_cde as brch_zip_cde,
trim(fi_brch_city_nme) as city,
trim(fi_brch_st_co_cde) as state,
md5( fi_brch_addr_1_txt || fi_brch_zip_cde) as brnch_id
from
tmp7 t1 left join
  (select 
    distinct on (system_id,fi_fund_group_nbr,financial_inst_id, fincl_inst_brch_id ) 
     system_id , 
     fi_brch_estb_dte, 
     fi_br_dflt_rep_id, 
     fi_brch_city_nme, 
     fi_brch_st_co_cde, 
     fi_brch_addr_1_txt, 
     fi_fund_group_nbr, 
     financial_inst_id, 
     fincl_inst_brch_id, 
     fi_brch_zip_cde 
   from g06 
   ) g on 
   (
    t1.system_id = g.system_id and
    t1.fund_grp_number = g.fi_fund_group_nbr and
    t1.dealer = g.financial_inst_id and 
    t1.dealerbrch=g.fincl_inst_brch_id
    )
)
DISTRIBUTED BY (system_id)

;



--select count(*) from tmp9
--129163940 

drop table if exists tmp9;
create temp table tmp9 as (
with reps_lkp as (
select
distinct 
rep_crd,
fi_fund_group_nbr,
dealer,
dealerbrch,
repnumber
from
dev_mdr.branch_rep
where
rep_crd <> ''
)
select 
t1.*,
rep_crd
from
tmp8 t1 left join
reps_lkp  t2 on ( 
t1.fund_grp_number = t2.fi_fund_group_nbr 
and t1.dealer = t2.dealer
and t1.dealerbrch = t2.dealerbrch
and t1.repnumber = t2.repnumber
) 
)
DISTRIBUTED BY (system_id)

;


drop table if exists tmp10;
create temp table tmp10 as (
with mstar as (
select
distinct on (cusip)
*
from
morningstar.morningstar
)
select
t1.*,
globalcategoryname,
shareclasstype,
fundid,
investmentvehiclename,
fundfamilyname,
actual12b1,
trailingreturnytd,
trailingreturny1,
annualreportnetexpenseratio,
ratingoverall,
ristratingoverall
from
tmp9 t1  join
mstar t3 on (t1.sec_iss_id = t3.cusip)
)
DISTRIBUTED BY (system_id)
;


drop table if exists jameslondal.ff_holdings;
create table  jameslondal.ff_holdings as (
 select
 tmp10.hld_id,
 tmp10.brnch_id as office_id,
 tmp10.firm_name,
 tmp10.sub_accounting,
 tmp10.rep_crd as rep_id,
 tmp10.account,
 tmp10.mutual_fund_lng_nm,
 tmp10.sec_iss_id as cusip,
 case when nscc_social_code is null then nscc_social_code_oh5 else nscc_social_code end as social_code,
 globalcategoryname,
 shareclasstype,
 fundid,
 investmentvehiclename,
 fundfamilyname,
 actual12b1,
 trailingreturnytd,
 trailingreturny1,
 annualreportnetexpenseratio,
 ratingoverall,
 ristratingoverall,
 city,
 state,
 brch_zip_cde,
 round((totshrsiss + totshrsuiss)*price,2) as aum
 from
 tmp10  join (
                         select 
                         system_id,
                         fund,
                         price
                         from
                         gt4
                         where
                         (price_date, system_id, fund) in (
                                                             select
                                                             max(price_date) as price_date,
                                                             system_id,
                                                             fund
                                                             from
                                                             gt4
                                                             group by
                                                             system_id,
                                                             fund
                                                            ) 
                         )
 t1 on (
        tmp10.system_id = t1.system_id and
        tmp10.fund_code = t1.fund
       )

 
) 
DISTRIBUTED BY (hld_id)
;


----------------------- Social Code Mappings by Account A88 (transactions)-------------------------
set search_path to assetmgt;

--125738158 
drop table if exists tmp1;
create temp table tmp1 as (
select
trade_date,
md5(a88.system_id || a88.account || j72.fund_code ) as hld_id, 
a88.system_id,
a88.repnumber,
a88.account,
a88.state_code,
j72.mgmt_code,
j72.fund_sponsor_id,
j72.mutual_fund_lng_nm,
j72.master_mf_id,
j72.fund_code,
j72.sec_iss_id,
j72.ticker_symbol_id,
a88.socialcode as ta_socialcode,
j68.fund_sponsor_nme,
case when fs_opr_mode_cd = 'M' then 1 else 0 end as sub_accounting,
dealer,
dealerbrch,
count(*) as transactions,
sum(case when balance_indicator = 'A' then round(gross_amount,2) else 0.00 end) as inflows,
sum(case when balance_indicator = 'S' then round(gross_amount,2) else 0.00 end) as outflows,
sum(case when trans_code in (11) then round(gross_amount,2) else 0.00 end) as income_reinvest,
sum(case when trans_code in (13) then round(gross_amount,2) else 0.00 end) as income_out
from
a88 join 
j72 on (a88.fund = j72.fund_code and a88.system_id = j72.system_id) join 
j68 on (j72.fund_sponsor_id = j68.fund_sponsor_id and j72.system_id = j68.system_id)
where
trade_date between date_trunc('month', current_date) - interval '7 month' and date_trunc('month', current_date)- interval '1 day'
and gross_amount > 0
and sec_iss_id <> ''
group by
trade_date,
md5(a88.system_id || a88.account || j72.fund_code ), 
a88.system_id,
a88.repnumber,
a88.account,
a88.state_code,
j72.mgmt_code,
j72.fund_sponsor_id,
j72.mutual_fund_lng_nm,
j72.master_mf_id,
j72.fund_code,
j72.sec_iss_id,
j72.ticker_symbol_id,
a88.socialcode,
j68.fund_sponsor_nme,
case when fs_opr_mode_cd = 'M' then 1 else 0 end ,
dealer,
dealerbrch
order by count(*) desc
)
DISTRIBUTED BY (system_id)
;

--125738158 
drop table if exists tmp2;
create temp table  tmp2 as
select
t1.*,
case 
  when oh6.nscc_social_code = '02' then '01' -- old women social code that no longer exists
  when oh6.nscc_social_code = '10' then '34' -- no longer exists
  else oh6.nscc_social_code
end as nscc_social_code
from
tmp1 t1 left join
oh6 on (
t1.system_id = oh6.system_id and
t1.ta_socialcode = oh6.social_code and
t1.mgmt_code = oh6.mgmt_code and
nscc_default_cd = 'Y'
)
DISTRIBUTED BY (system_id)

;

--select count(*) from tmp3
--125738158  

drop table if exists tmp3;
create temp table  tmp3 as (
with oh5_l as (
select
distinct 
system_id,
dflt_social_code,
nscc_default_cd,
nscc_social_code
from
oh5
where
nscc_default_cd = 'Y'
)
select
t1.*,
case 
  when oh5_l.nscc_social_code = '02' then '01' -- old women social code that no longer exists
  when oh5_l.nscc_social_code = '10' then '34' -- no longer exists
  else oh5_l.nscc_social_code
end as nscc_social_code_oh5
from
tmp2 t1 left join
oh5_l on (
t1.system_id = oh5_l.system_id and
t1.ta_socialcode = oh5_l.dflt_social_code 
)
) 
DISTRIBUTED BY (system_id)
;

--select count(*) from tmp4
--129163940 

drop table if exists tmp4;
create temp table  tmp4 as (
with bn2_l as (
select distinct
system_id,
social_code,
fund_sponsor_id,
agent_id,
plan_type_cde,
social_cd_house_acct_cd
from
bn2
)
select
t1.*,
agent_id,
plan_type_cde,
social_cd_house_acct_cd
from
tmp3 t1 left join
bn2_l on (
t1.system_id = bn2_l.system_id and
t1.ta_socialcode = bn2_l.social_code and
t1.fund_sponsor_id = bn2_l.fund_sponsor_id
)
)
DISTRIBUTED BY (system_id)
;

--select count(*) from tmp6
--129163940 

drop table if exists tmp6;
create temp table  tmp6 as (
select
t1.*,
d.fund_grp_number
from
tmp4 t1 left join
 (select 
 distinct 
  a.system_id, 
  a.fund_grp_number, 
  a.fund_code 
 from d60 a inner join 
 assetmgt.d61 b on a.fund_grp_number = b.fund_grp_number and b.fund_grp_class_cde = 4) d on 
   (
    t1.system_id = d.system_id and
    t1.fund_code = d.fund_code
    )
)
DISTRIBUTED BY (system_id)   
;

--select count(*) from tmp7
--129163940 

drop table if exists tmp7;
create temp table  tmp7 as (
select
t1.*,
g.fincl_inst_name_1 as firm_name
from
tmp6 t1 left join
 (select distinct system_id, fi_fund_group_nbr, financial_inst_id, fincl_inst_name_1 from g02 where financial_inst_id is not null) g on 
   (
    t1.system_id = g.system_id and
    t1.fund_grp_number = g.fi_fund_group_nbr and
    t1.dealer = g.financial_inst_id
    )
) 
DISTRIBUTED BY (system_id)

;

--select count(*) from tmp8
--129163940 

drop table if exists tmp8;
create temp table tmp8 as (
select
t1.*,
g.fi_brch_zip_cde as brch_zip_cde,
trim(fi_brch_city_nme) as city,
trim(fi_brch_st_co_cde) as state,
md5( fi_brch_addr_1_txt || fi_brch_zip_cde) as brnch_id
from
tmp7 t1 left join
  (select 
    distinct on (system_id,fi_fund_group_nbr,financial_inst_id, fincl_inst_brch_id ) 
     system_id , 
     fi_brch_estb_dte, 
     fi_br_dflt_rep_id, 
     fi_brch_city_nme, 
     fi_brch_st_co_cde, 
     fi_brch_addr_1_txt, 
     fi_fund_group_nbr, 
     financial_inst_id, 
     fincl_inst_brch_id, 
     fi_brch_zip_cde 
   from g06 
   ) g on 
   (
    t1.system_id = g.system_id and
    t1.fund_grp_number = g.fi_fund_group_nbr and
    t1.dealer = g.financial_inst_id and 
    t1.dealerbrch=g.fincl_inst_brch_id
    )
)
DISTRIBUTED BY (system_id)

;



--select count(*) from tmp9
--129163940 

drop table if exists tmp9;
create temp table tmp9 as (
with reps_lkp as (
select
distinct 
rep_crd,
fi_fund_group_nbr,
dealer,
dealerbrch,
repnumber
from
dev_mdr.branch_rep
where
rep_crd <> ''
)
select 
t1.*,
rep_crd
from
tmp8 t1 left join
reps_lkp  t2 on ( 
t1.fund_grp_number = t2.fi_fund_group_nbr 
and t1.dealer = t2.dealer
and t1.dealerbrch = t2.dealerbrch
and t1.repnumber = t2.repnumber
) 
)
DISTRIBUTED BY (system_id)

;


drop table if exists tmp10;
create temp table tmp10 as (
with mstar as (
select
distinct on (cusip)
*
from
morningstar.morningstar
)
select
t1.*,
globalcategoryname,
shareclasstype,
fundid,
investmentvehiclename,
fundfamilyname,
actual12b1,
trailingreturnytd,
trailingreturny1,
annualreportnetexpenseratio,
ratingoverall,
ristratingoverall
from
tmp9 t1  join
mstar t3 on (t1.sec_iss_id = t3.cusip)
)
DISTRIBUTED BY (system_id)
;


drop table if exists jameslondal.ff_transactions;
create table  jameslondal.ff_transactions as
(select
 trade_date, 
 hld_id,
 inflows - income_reinvest as gross_sales,
 outflows - income_out as redemptions,
 (inflows - income_reinvest) - (outflows - income_out) as net_sales,
 income_reinvest,
 income_out,
 brnch_id as office_id,
 sub_accounting,
 rep_crd as rep_id,
 account,
 state_code,
 sec_iss_id as cusip,
 case when nscc_social_code is null then nscc_social_code_oh5 else nscc_social_code end as social_code,
 globalcategoryname,
 shareclasstype,
 fundid,
 investmentvehiclename,
 fundfamilyname,
 actual12b1,
 trailingreturnytd,
 trailingreturny1,
 annualreportnetexpenseratio,
 ratingoverall,
 ristratingoverall as riskratingoverall,
 brch_zip_cde,
 firm_name,
 city,
 state
from
tmp10 
where 
case when nscc_social_code is null then nscc_social_code_oh5 else nscc_social_code end is not null 
)
DISTRIBUTED BY (hld_id)
;

------------------------Summary Tables ---------------


drop table if exists jameslondal.ff_summary_fund_family;
create table jameslondal.ff_summary_fund_family as (
select
fundfamilyname,
trade_date,
investmentvehiclename,
ratingoverall,
shareclasstype,
case 
when t1.social_code = '01' then 'Individual/Joint/Savings'  
when t1.social_code = '02' then 'Individual/Joint/Savings'  
when t1.social_code = '03' then 'Individual/Joint/Savings'  
when t1.social_code = '04' then 'Corporations'  
when t1.social_code = '05' then 'Misc'  
when t1.social_code = '06' then 'Broker/Dealer'  
when t1.social_code = '07' then 'Bank/Trust'  
when t1.social_code = '08' then 'Retirement'  
when t1.social_code = '09' then 'Retirement'  
when t1.social_code = '10' then ''  
when t1.social_code = '11' then 'Retirement'  
when t1.social_code = '12' then 'Retirement'  
when t1.social_code = '13' then 'Retirement'  
when t1.social_code = '14' then 'Misc'  
when t1.social_code = '15' then 'Trust'  
when t1.social_code = '16' then 'Individual/Joint/Savings'  
when t1.social_code = '17' then 'Retirement'  
when t1.social_code = '18' then 'Broker/Dealer'  
when t1.social_code = '19' then 'Retirement'  
when t1.social_code = '20' then 'Retirement'  
when t1.social_code = '21' then 'Non-Profit'  
when t1.social_code = '22' then 'Retirement'  
when t1.social_code = '23' then 'Retirement'  
when t1.social_code = '24' then 'Retirement'  
when t1.social_code = '25' then 'Retirement'  
when t1.social_code = '26' then 'Misc'  
when t1.social_code = '27' then 'Misc'  
when t1.social_code = '28' then 'Retirement'  
when t1.social_code = '29' then 'Retirement'  
when t1.social_code = '30' then 'Retirement'  
when t1.social_code = '31' then 'Retirement'  
when t1.social_code = '32' then 'Retirement'  
when t1.social_code = '33' then 'Retirement'  
when t1.social_code = '34' then 'Retirement'  
when t1.social_code = '35' then 'Retirement'  
when t1.social_code = '36' then 'Retirement'  
when t1.social_code = '37' then 'Individual/Joint/Savings'  
when t1.social_code = '38' then 'Individual/Joint/Savings'  
when t1.social_code = '39' then 'Misc'  
when t1.social_code = '40' then 'Misc'  
when t1.social_code = '41' then 'Misc'  
when t1.social_code = '42' then 'Trust'  
when t1.social_code = '43' then 'Retirement'  
when t1.social_code = '44' then 'Retirement'  
when t1.social_code = '45' then 'Retirement'  
when t1.social_code = '46' then 'Retirement'  
when t1.social_code = '47' then 'Individual/Joint/Savings'  
when t1.social_code = '48' then 'Individual/Joint/Savings'  
when t1.social_code = '49' then 'Retirement'  
when t1.social_code = '50' then 'Retirement'  
when t1.social_code = '51' then 'Retirement'  
when t1.social_code = '52' then 'Individual/Joint/Savings'  
when t1.social_code = '53' then 'Retirement'  
when t1.social_code = '54' then 'Retirement'  
when t1.social_code = '55' then 'Misc'  
when t1.social_code = '56' then 'Misc'  
when t1.social_code = '57' then 'Retirement'  
when t1.social_code = '58' then 'Retirement'  
when t1.social_code = '59' then 'Retirement'  
when t1.social_code = '60' then 'Retirement'  
when t1.social_code = '61' then 'Misc'  
when t1.social_code = '62' then 'Misc'  
when t1.social_code = '63' then 'Corporations'  
when t1.social_code = '64' then 'Retirement'  
when t1.social_code = '65' then 'Individual/Joint/Savings'  
when t1.social_code = '66' then 'Individual/Joint/Savings'  
when t1.social_code = '67' then 'Misc'  
when t1.social_code = '68' then 'Misc'  
when t1.social_code = '69' then 'Retirement'  
when t1.social_code = '70' then 'Retirement'  
when t1.social_code = '71' then 'Retirement'  
when t1.social_code = '72' then 'Retirement'  
when t1.social_code = '73' then 'Retirement'  
when t1.social_code = '74' then 'Misc'  
when t1.social_code = '75' then 'Misc'  
when t1.social_code = '76' then 'Retirement'  
when t1.social_code = '77' then 'Retirement'  
when t1.social_code = '78' then 'Retirement'  
when t1.social_code = '79' then 'Retirement'  
when t1.social_code = '80' then 'Retirement'  
when t1.social_code = '81' then 'Individual/Joint/Savings'  
when t1.social_code = '82' then 'Retirement'  
when t1.social_code = '83' then 'Misc'  
when t1.social_code = '84' then 'Individual/Joint/Savings'  
when t1.social_code = '85' then 'Misc'  
when t1.social_code = '86' then 'Retirement'  
when t1.social_code = '87' then 'Retirement'  
when t1.social_code = '88' then 'Retirement'  
when t1.social_code = '89' then 'Retirement'  
when t1.social_code = '90' then 'Retirement'  
when t1.social_code = '91' then 'Retirement'  
when t1.social_code = '92' then 'Retirement'  
when t1.social_code = '93' then 'Retirement'  
when t1.social_code = '94' then 'Misc'  
when t1.social_code = '95' then 'Misc'  
when t1.social_code = '96' then 'Misc'  
when t1.social_code = '97' then 'Misc'  
when t1.social_code = '98' then 'Misc'  
when t1.social_code = '99' then 'Misc'  
when t1.social_code = '99' then 'Misc'  
else t1.social_code
end as social_code,
sum(gross_sales) as gross_sales,
sum(redemptions) as redemptions,
sum(net_sales) as net_sales
from
jameslondal.ff_transactions t1
where
state in ('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN',
'IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ',
'NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA',
'WA','WV','WI','WY')
and ratingoverall > 0
group by
trade_date,
fundfamilyname,
investmentvehiclename,
ratingoverall,
shareclasstype,
case 
when t1.social_code = '01' then 'Individual/Joint/Savings'  
when t1.social_code = '02' then 'Individual/Joint/Savings'  
when t1.social_code = '03' then 'Individual/Joint/Savings'  
when t1.social_code = '04' then 'Corporations'  
when t1.social_code = '05' then 'Misc'  
when t1.social_code = '06' then 'Broker/Dealer'  
when t1.social_code = '07' then 'Bank/Trust'  
when t1.social_code = '08' then 'Retirement'  
when t1.social_code = '09' then 'Retirement'  
when t1.social_code = '10' then ''  
when t1.social_code = '11' then 'Retirement'  
when t1.social_code = '12' then 'Retirement'  
when t1.social_code = '13' then 'Retirement'  
when t1.social_code = '14' then 'Misc'  
when t1.social_code = '15' then 'Trust'  
when t1.social_code = '16' then 'Individual/Joint/Savings'  
when t1.social_code = '17' then 'Retirement'  
when t1.social_code = '18' then 'Broker/Dealer'  
when t1.social_code = '19' then 'Retirement'  
when t1.social_code = '20' then 'Retirement'  
when t1.social_code = '21' then 'Non-Profit'  
when t1.social_code = '22' then 'Retirement'  
when t1.social_code = '23' then 'Retirement'  
when t1.social_code = '24' then 'Retirement'  
when t1.social_code = '25' then 'Retirement'  
when t1.social_code = '26' then 'Misc'  
when t1.social_code = '27' then 'Misc'  
when t1.social_code = '28' then 'Retirement'  
when t1.social_code = '29' then 'Retirement'  
when t1.social_code = '30' then 'Retirement'  
when t1.social_code = '31' then 'Retirement'  
when t1.social_code = '32' then 'Retirement'  
when t1.social_code = '33' then 'Retirement'  
when t1.social_code = '34' then 'Retirement'  
when t1.social_code = '35' then 'Retirement'  
when t1.social_code = '36' then 'Retirement'  
when t1.social_code = '37' then 'Individual/Joint/Savings'  
when t1.social_code = '38' then 'Individual/Joint/Savings'  
when t1.social_code = '39' then 'Misc'  
when t1.social_code = '40' then 'Misc'  
when t1.social_code = '41' then 'Misc'  
when t1.social_code = '42' then 'Trust'  
when t1.social_code = '43' then 'Retirement'  
when t1.social_code = '44' then 'Retirement'  
when t1.social_code = '45' then 'Retirement'  
when t1.social_code = '46' then 'Retirement'  
when t1.social_code = '47' then 'Individual/Joint/Savings'  
when t1.social_code = '48' then 'Individual/Joint/Savings'  
when t1.social_code = '49' then 'Retirement'  
when t1.social_code = '50' then 'Retirement'  
when t1.social_code = '51' then 'Retirement'  
when t1.social_code = '52' then 'Individual/Joint/Savings'  
when t1.social_code = '53' then 'Retirement'  
when t1.social_code = '54' then 'Retirement'  
when t1.social_code = '55' then 'Misc'  
when t1.social_code = '56' then 'Misc'  
when t1.social_code = '57' then 'Retirement'  
when t1.social_code = '58' then 'Retirement'  
when t1.social_code = '59' then 'Retirement'  
when t1.social_code = '60' then 'Retirement'  
when t1.social_code = '61' then 'Misc'  
when t1.social_code = '62' then 'Misc'  
when t1.social_code = '63' then 'Corporations'  
when t1.social_code = '64' then 'Retirement'  
when t1.social_code = '65' then 'Individual/Joint/Savings'  
when t1.social_code = '66' then 'Individual/Joint/Savings'  
when t1.social_code = '67' then 'Misc'  
when t1.social_code = '68' then 'Misc'  
when t1.social_code = '69' then 'Retirement'  
when t1.social_code = '70' then 'Retirement'  
when t1.social_code = '71' then 'Retirement'  
when t1.social_code = '72' then 'Retirement'  
when t1.social_code = '73' then 'Retirement'  
when t1.social_code = '74' then 'Misc'  
when t1.social_code = '75' then 'Misc'  
when t1.social_code = '76' then 'Retirement'  
when t1.social_code = '77' then 'Retirement'  
when t1.social_code = '78' then 'Retirement'  
when t1.social_code = '79' then 'Retirement'  
when t1.social_code = '80' then 'Retirement'  
when t1.social_code = '81' then 'Individual/Joint/Savings'  
when t1.social_code = '82' then 'Retirement'  
when t1.social_code = '83' then 'Misc'  
when t1.social_code = '84' then 'Individual/Joint/Savings'  
when t1.social_code = '85' then 'Misc'  
when t1.social_code = '86' then 'Retirement'  
when t1.social_code = '87' then 'Retirement'  
when t1.social_code = '88' then 'Retirement'  
when t1.social_code = '89' then 'Retirement'  
when t1.social_code = '90' then 'Retirement'  
when t1.social_code = '91' then 'Retirement'  
when t1.social_code = '92' then 'Retirement'  
when t1.social_code = '93' then 'Retirement'  
when t1.social_code = '94' then 'Misc'  
when t1.social_code = '95' then 'Misc'  
when t1.social_code = '96' then 'Misc'  
when t1.social_code = '97' then 'Misc'  
when t1.social_code = '98' then 'Misc'  
when t1.social_code = '99' then 'Misc'  
when t1.social_code = '99' then 'Misc'  
else t1.social_code
end
)
DISTRIBUTED BY (fundfamilyname)
;


drop table if exists jameslondal.ff_summary_fund_family_public;
create table jameslondal.ff_summary_fund_family_public as (
select
globalcategoryname,
trade_date,
ratingoverall,
shareclasstype,
case 
when t1.social_code = '01' then 'Individual/Joint/Savings'  
when t1.social_code = '02' then 'Individual/Joint/Savings'  
when t1.social_code = '03' then 'Individual/Joint/Savings'  
when t1.social_code = '04' then 'Corporations'  
when t1.social_code = '05' then 'Misc'  
when t1.social_code = '06' then 'Broker/Dealer'  
when t1.social_code = '07' then 'Bank/Trust'  
when t1.social_code = '08' then 'Retirement'  
when t1.social_code = '09' then 'Retirement'  
when t1.social_code = '10' then ''  
when t1.social_code = '11' then 'Retirement'  
when t1.social_code = '12' then 'Retirement'  
when t1.social_code = '13' then 'Retirement'  
when t1.social_code = '14' then 'Misc'  
when t1.social_code = '15' then 'Trust'  
when t1.social_code = '16' then 'Individual/Joint/Savings'  
when t1.social_code = '17' then 'Retirement'  
when t1.social_code = '18' then 'Broker/Dealer'  
when t1.social_code = '19' then 'Retirement'  
when t1.social_code = '20' then 'Retirement'  
when t1.social_code = '21' then 'Non-Profit'  
when t1.social_code = '22' then 'Retirement'  
when t1.social_code = '23' then 'Retirement'  
when t1.social_code = '24' then 'Retirement'  
when t1.social_code = '25' then 'Retirement'  
when t1.social_code = '26' then 'Misc'  
when t1.social_code = '27' then 'Misc'  
when t1.social_code = '28' then 'Retirement'  
when t1.social_code = '29' then 'Retirement'  
when t1.social_code = '30' then 'Retirement'  
when t1.social_code = '31' then 'Retirement'  
when t1.social_code = '32' then 'Retirement'  
when t1.social_code = '33' then 'Retirement'  
when t1.social_code = '34' then 'Retirement'  
when t1.social_code = '35' then 'Retirement'  
when t1.social_code = '36' then 'Retirement'  
when t1.social_code = '37' then 'Individual/Joint/Savings'  
when t1.social_code = '38' then 'Individual/Joint/Savings'  
when t1.social_code = '39' then 'Misc'  
when t1.social_code = '40' then 'Misc'  
when t1.social_code = '41' then 'Misc'  
when t1.social_code = '42' then 'Trust'  
when t1.social_code = '43' then 'Retirement'  
when t1.social_code = '44' then 'Retirement'  
when t1.social_code = '45' then 'Retirement'  
when t1.social_code = '46' then 'Retirement'  
when t1.social_code = '47' then 'Individual/Joint/Savings'  
when t1.social_code = '48' then 'Individual/Joint/Savings'  
when t1.social_code = '49' then 'Retirement'  
when t1.social_code = '50' then 'Retirement'  
when t1.social_code = '51' then 'Retirement'  
when t1.social_code = '52' then 'Individual/Joint/Savings'  
when t1.social_code = '53' then 'Retirement'  
when t1.social_code = '54' then 'Retirement'  
when t1.social_code = '55' then 'Misc'  
when t1.social_code = '56' then 'Misc'  
when t1.social_code = '57' then 'Retirement'  
when t1.social_code = '58' then 'Retirement'  
when t1.social_code = '59' then 'Retirement'  
when t1.social_code = '60' then 'Retirement'  
when t1.social_code = '61' then 'Misc'  
when t1.social_code = '62' then 'Misc'  
when t1.social_code = '63' then 'Corporations'  
when t1.social_code = '64' then 'Retirement'  
when t1.social_code = '65' then 'Individual/Joint/Savings'  
when t1.social_code = '66' then 'Individual/Joint/Savings'  
when t1.social_code = '67' then 'Misc'  
when t1.social_code = '68' then 'Misc'  
when t1.social_code = '69' then 'Retirement'  
when t1.social_code = '70' then 'Retirement'  
when t1.social_code = '71' then 'Retirement'  
when t1.social_code = '72' then 'Retirement'  
when t1.social_code = '73' then 'Retirement'  
when t1.social_code = '74' then 'Misc'  
when t1.social_code = '75' then 'Misc'  
when t1.social_code = '76' then 'Retirement'  
when t1.social_code = '77' then 'Retirement'  
when t1.social_code = '78' then 'Retirement'  
when t1.social_code = '79' then 'Retirement'  
when t1.social_code = '80' then 'Retirement'  
when t1.social_code = '81' then 'Individual/Joint/Savings'  
when t1.social_code = '82' then 'Retirement'  
when t1.social_code = '83' then 'Misc'  
when t1.social_code = '84' then 'Individual/Joint/Savings'  
when t1.social_code = '85' then 'Misc'  
when t1.social_code = '86' then 'Retirement'  
when t1.social_code = '87' then 'Retirement'  
when t1.social_code = '88' then 'Retirement'  
when t1.social_code = '89' then 'Retirement'  
when t1.social_code = '90' then 'Retirement'  
when t1.social_code = '91' then 'Retirement'  
when t1.social_code = '92' then 'Retirement'  
when t1.social_code = '93' then 'Retirement'  
when t1.social_code = '94' then 'Misc'  
when t1.social_code = '95' then 'Misc'  
when t1.social_code = '96' then 'Misc'  
when t1.social_code = '97' then 'Misc'  
when t1.social_code = '98' then 'Misc'  
when t1.social_code = '99' then 'Misc'  
when t1.social_code = '99' then 'Misc'  
else t1.social_code
end as social_code,
sum(gross_sales) as gross_sales,
sum(redemptions) as redemptions,
sum(net_sales) as net_sales
from
jameslondal.ff_transactions t1
where
state in ('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN',
'IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ',
'NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA',
'WA','WV','WI','WY')
and ratingoverall > 0
group by
trade_date,
globalcategoryname,
ratingoverall,
shareclasstype,
case 
when t1.social_code = '01' then 'Individual/Joint/Savings'  
when t1.social_code = '02' then 'Individual/Joint/Savings'  
when t1.social_code = '03' then 'Individual/Joint/Savings'  
when t1.social_code = '04' then 'Corporations'  
when t1.social_code = '05' then 'Misc'  
when t1.social_code = '06' then 'Broker/Dealer'  
when t1.social_code = '07' then 'Bank/Trust'  
when t1.social_code = '08' then 'Retirement'  
when t1.social_code = '09' then 'Retirement'  
when t1.social_code = '10' then ''  
when t1.social_code = '11' then 'Retirement'  
when t1.social_code = '12' then 'Retirement'  
when t1.social_code = '13' then 'Retirement'  
when t1.social_code = '14' then 'Misc'  
when t1.social_code = '15' then 'Trust'  
when t1.social_code = '16' then 'Individual/Joint/Savings'  
when t1.social_code = '17' then 'Retirement'  
when t1.social_code = '18' then 'Broker/Dealer'  
when t1.social_code = '19' then 'Retirement'  
when t1.social_code = '20' then 'Retirement'  
when t1.social_code = '21' then 'Non-Profit'  
when t1.social_code = '22' then 'Retirement'  
when t1.social_code = '23' then 'Retirement'  
when t1.social_code = '24' then 'Retirement'  
when t1.social_code = '25' then 'Retirement'  
when t1.social_code = '26' then 'Misc'  
when t1.social_code = '27' then 'Misc'  
when t1.social_code = '28' then 'Retirement'  
when t1.social_code = '29' then 'Retirement'  
when t1.social_code = '30' then 'Retirement'  
when t1.social_code = '31' then 'Retirement'  
when t1.social_code = '32' then 'Retirement'  
when t1.social_code = '33' then 'Retirement'  
when t1.social_code = '34' then 'Retirement'  
when t1.social_code = '35' then 'Retirement'  
when t1.social_code = '36' then 'Retirement'  
when t1.social_code = '37' then 'Individual/Joint/Savings'  
when t1.social_code = '38' then 'Individual/Joint/Savings'  
when t1.social_code = '39' then 'Misc'  
when t1.social_code = '40' then 'Misc'  
when t1.social_code = '41' then 'Misc'  
when t1.social_code = '42' then 'Trust'  
when t1.social_code = '43' then 'Retirement'  
when t1.social_code = '44' then 'Retirement'  
when t1.social_code = '45' then 'Retirement'  
when t1.social_code = '46' then 'Retirement'  
when t1.social_code = '47' then 'Individual/Joint/Savings'  
when t1.social_code = '48' then 'Individual/Joint/Savings'  
when t1.social_code = '49' then 'Retirement'  
when t1.social_code = '50' then 'Retirement'  
when t1.social_code = '51' then 'Retirement'  
when t1.social_code = '52' then 'Individual/Joint/Savings'  
when t1.social_code = '53' then 'Retirement'  
when t1.social_code = '54' then 'Retirement'  
when t1.social_code = '55' then 'Misc'  
when t1.social_code = '56' then 'Misc'  
when t1.social_code = '57' then 'Retirement'  
when t1.social_code = '58' then 'Retirement'  
when t1.social_code = '59' then 'Retirement'  
when t1.social_code = '60' then 'Retirement'  
when t1.social_code = '61' then 'Misc'  
when t1.social_code = '62' then 'Misc'  
when t1.social_code = '63' then 'Corporations'  
when t1.social_code = '64' then 'Retirement'  
when t1.social_code = '65' then 'Individual/Joint/Savings'  
when t1.social_code = '66' then 'Individual/Joint/Savings'  
when t1.social_code = '67' then 'Misc'  
when t1.social_code = '68' then 'Misc'  
when t1.social_code = '69' then 'Retirement'  
when t1.social_code = '70' then 'Retirement'  
when t1.social_code = '71' then 'Retirement'  
when t1.social_code = '72' then 'Retirement'  
when t1.social_code = '73' then 'Retirement'  
when t1.social_code = '74' then 'Misc'  
when t1.social_code = '75' then 'Misc'  
when t1.social_code = '76' then 'Retirement'  
when t1.social_code = '77' then 'Retirement'  
when t1.social_code = '78' then 'Retirement'  
when t1.social_code = '79' then 'Retirement'  
when t1.social_code = '80' then 'Retirement'  
when t1.social_code = '81' then 'Individual/Joint/Savings'  
when t1.social_code = '82' then 'Retirement'  
when t1.social_code = '83' then 'Misc'  
when t1.social_code = '84' then 'Individual/Joint/Savings'  
when t1.social_code = '85' then 'Misc'  
when t1.social_code = '86' then 'Retirement'  
when t1.social_code = '87' then 'Retirement'  
when t1.social_code = '88' then 'Retirement'  
when t1.social_code = '89' then 'Retirement'  
when t1.social_code = '90' then 'Retirement'  
when t1.social_code = '91' then 'Retirement'  
when t1.social_code = '92' then 'Retirement'  
when t1.social_code = '93' then 'Retirement'  
when t1.social_code = '94' then 'Misc'  
when t1.social_code = '95' then 'Misc'  
when t1.social_code = '96' then 'Misc'  
when t1.social_code = '97' then 'Misc'  
when t1.social_code = '98' then 'Misc'  
when t1.social_code = '99' then 'Misc'  
when t1.social_code = '99' then 'Misc'  
else t1.social_code
end
) 
DISTRIBUTED BY (globalcategoryname)

 ;
drop table if exists jameslondal.ff_region;
create table  jameslondal.ff_region as (
select
fundfamilyname,
globalcategoryname as sector,
state,
date_trunc('month',trade_date)::text as trade_date,
coalesce(sum(gross_sales),0)::text as gross_sales,
coalesce(sum(redemptions),0)::text as redemptions,
coalesce(sum(net_sales ),0)::text as net_sales,
coalesce(sum(income_reinvest),0)::text as income_reinvest
from
jameslondal.ff_transactions
where
state in ('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN',
'IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ',
'NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA',
'WA','WV','WI','WY')
and ratingoverall > 0
group by
fundfamilyname,
globalcategoryname,
state,
date_trunc('month',trade_date)::text
)
DISTRIBUTED BY (fundfamilyname)
;


drop table if exists jameslondal.ff_region_public;
create table  jameslondal.ff_region_public as (
select
globalcategoryname as sector,
state,
date_trunc('month',trade_date)::text as trade_date,
coalesce(sum(gross_sales),0)::text as gross_sales,
coalesce(sum(redemptions),0)::text as redemptions,
coalesce(sum(net_sales ),0)::text as net_sales,
coalesce(sum(income_reinvest),0)::text as income_reinvest
from
jameslondal.ff_transactions
where
state in ('AL','AK','AZ','AR','CA','CO','CT','DE','FL','GA','HI','ID','IL','IN',
'IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO','MT','NE','NV','NH','NJ',
'NM','NY','NC','ND','OH','OK','OR','PA','RI','SC','SD','TN','TX','UT','VT','VA',
'WA','WV','WI','WY')
and ratingoverall > 0
group by
fundfamilyname,
globalcategoryname,
state,
date_trunc('month',trade_date)::text
)
DISTRIBUTED BY (sector)
;

----------------------- agrs -------------------------





drop table if exists jameslondal.ff_state_sector_aum;
create table  jameslondal.ff_state_sector_aum as (
select
globalcategoryname,
state,
sum(aum)as aum,
social_code
from
jameslondal.ff_holdings 
where 
state in ('AL',
'AK',
'AZ',
'AR',
'CA',
'CO',
'CT',
'DE',
'FL',
'GA',
'HI',
'ID',
'IL',
'IN',
'IA',
'KS',
'KY',
'LA',
'ME',
'MD',
'MA',
'MI',
'MN',
'MS',
'MO',
'MT',
'NE',
'NV',
'NH',
'NJ',
'NM',
'NY',
'NC',
'ND',
'OH',
'OK',
'OR',
'PA',
'RI',
'SC',
'SD',
'TN',
'TX',
'UT',
'VT',
'VA',
'WA',
'WV',
'WI',
'WY')
group by
globalcategoryname,
state,
social_code
order by
sum(aum) desc 
);


drop table if exists jameslondal.ff_myflows;
create table jameslondal.ff_myflows as (
select
fundfamilyname,
globalcategoryname as sector,
state,
date_trunc('month',trade_date)::text as trade_date,
coalesce(sum(gross_sales),0)::text as gross_sales,
coalesce(sum(redemptions),0)::text as redemptions,
coalesce(sum(net_sales ),0)::text as net_sales,
coalesce(sum(income_reinvest),0)::text as income_reinvest
from
jameslondal.ff_transactions 
where
state in ('AL',
'AK',
'AZ',
'AR',
'CA',
'CO',
'CT',
'DE',
'FL',
'GA',
'HI',
'ID',
'IL',
'IN',
'IA',
'KS',
'KY',
'LA',
'ME',
'MD',
'MA',
'MI',
'MN',
'MS',
'MO',
'MT',
'NE',
'NV',
'NH',
'NJ',
'NM',
'NY',
'NC',
'ND',
'OH',
'OK',
'OR',
'PA',
'RI',
'SC',
'SD',
'TN',
'TX',
'UT',
'VT',
'VA',
'WA',
'WV',
'WI',
'WY')
group by
fundfamilyname,
globalcategoryname,
state,
date_trunc('month',trade_date)::text
);

drop table if exists jameslondal.ff_preformance;
create table jameslondal.ff_preformance as (
select
fundfamilyname,
globalcategoryname,
date_trunc('month',trade_date)::text as trade_date,
coalesce(avg(trailingreturny1),0)::text as feeadj_return_y1,
coalesce(sum(gross_sales),0)::text as sales,
coalesce(sum(redemptions),0)::text as redemptions,
coalesce(sum(net_sales),0)::text as net_flow,
coalesce(sum(income_reinvest),0)::text as income_reinvest
from
jameslondal.ff_transactions t1 
where
state in ('AL',
'AK',
'AZ',
'AR',
'CA',
'CO',
'CT',
'DE',
'FL',
'GA',
'HI',
'ID',
'IL',
'IN',
'IA',
'KS',
'KY',
'LA',
'ME',
'MD',
'MA',
'MI',
'MN',
'MS',
'MO',
'MT',
'NE',
'NV',
'NH',
'NJ',
'NM',
'NY',
'NC',
'ND',
'OH',
'OK',
'OR',
'PA',
'RI',
'SC',
'SD',
'TN',
'TX',
'UT',
'VT',
'VA',
'WA',
'WV',
'WI',
'WY')
group by
fundfamilyname,
globalcategoryname,
date_trunc('month',trade_date)::text
);


drop table if exists jameslondal.ff_social_code_flows;
create table jameslondal.ff_social_code_flows as (
select
fundfamilyname,
investmentvehiclename,
date_trunc('month',trade_date)::text as trade_date,
social_code,
coalesce(avg(trailingreturny1),0)::text as feeadj_return_y1,
coalesce(sum(gross_sales),0)::text as sales,
coalesce(sum(redemptions),0)::text as redemptions,
coalesce(sum(net_sales),0)::text as net_flow,
coalesce(sum(income_reinvest),0)::text as income_reinvest
from
jameslondal.ff_transactions 
where
state in ('AL',
'AK',
'AZ',
'AR',
'CA',
'CO',
'CT',
'DE',
'FL',
'GA',
'HI',
'ID',
'IL',
'IN',
'IA',
'KS',
'KY',
'LA',
'ME',
'MD',
'MA',
'MI',
'MN',
'MS',
'MO',
'MT',
'NE',
'NV',
'NH',
'NJ',
'NM',
'NY',
'NC',
'ND',
'OH',
'OK',
'OR',
'PA',
'RI',
'SC',
'SD',
'TN',
'TX',
'UT',
'VT',
'VA',
'WA',
'WV',
'WI',
'WY')
group by
fundfamilyname,
investmentvehiclename,
date_trunc('month',trade_date)::text,
social_code
);


drop table if exists jameslondal.ff_master_dashboard2;
create table jameslondal.ff_master_dashboard2 as (
select
state,
globalcategoryname as sector,
date_trunc('month',trade_date)::date::text as trade_date,
social_code,
coalesce(sum(gross_sales),0)::text as sales,
coalesce(sum(redemptions),0)::text as redemptions,
coalesce(sum(net_sales),0)::text as net_flow,
coalesce(sum(income_reinvest),0)::text as income_reinvest
from
jameslondal.ff_transactions 
where
state in ('AL',
'AK',
'AZ',
'AR',
'CA',
'CO',
'CT',
'DE',
'FL',
'GA',
'HI',
'ID',
'IL',
'IN',
'IA',
'KS',
'KY',
'LA',
'ME',
'MD',
'MA',
'MI',
'MN',
'MS',
'MO',
'MT',
'NE',
'NV',
'NH',
'NJ',
'NM',
'NY',
'NC',
'ND',
'OH',
'OK',
'OR',
'PA',
'RI',
'SC',
'SD',
'TN',
'TX',
'UT',
'VT',
'VA',
'WA',
'WV',
'WI',
'WY')
group by
state,
globalcategoryname ,
date_trunc('month',trade_date)::date::text ,
social_code
);


drop table if exists jameslondal.ff_social_code_flows_sector;
create table jameslondal.ff_social_code_flows_sector as (
select
fundfamilyname,
globalcategoryname as sector,
date_trunc('month',trade_date)::text as trade_date,
social_code,
coalesce(sum(gross_sales),0)::text as sales,
coalesce(sum(redemptions),0)::text as redemptions,
coalesce(sum(net_sales),0)::text as net_flow,
coalesce(sum(income_reinvest),0)::text as income_reinvest
from
jameslondal.ff_transactions 
where
state in ('AL',
'AK',
'AZ',
'AR',
'CA',
'CO',
'CT',
'DE',
'FL',
'GA',
'HI',
'ID',
'IL',
'IN',
'IA',
'KS',
'KY',
'LA',
'ME',
'MD',
'MA',
'MI',
'MN',
'MS',
'MO',
'MT',
'NE',
'NV',
'NH',
'NJ',
'NM',
'NY',
'NC',
'ND',
'OH',
'OK',
'OR',
'PA',
'RI',
'SC',
'SD',
'TN',
'TX',
'UT',
'VT',
'VA',
'WA',
'WV',
'WI',
'WY')
group by
fundfamilyname,
globalcategoryname,
date_trunc('month',trade_date)::text,
social_code
);

drop table if exists jameslondal.ff_social_code_market_flows;
create table jameslondal.ff_social_code_market_flows as (
select
fundfamilyname,
date_trunc('month',trade_date)::text as trade_date,
case 
when t1.social_code = '01' then 'Individual/Joint/Savings'  
when t1.social_code = '02' then 'Individual/Joint/Savings'  
when t1.social_code = '03' then 'Individual/Joint/Savings'  
when t1.social_code = '04' then 'Corporations'  
when t1.social_code = '05' then 'Misc'  
when t1.social_code = '06' then 'Broker/Dealer'  
when t1.social_code = '07' then 'Bank/Trust'  
when t1.social_code = '08' then 'Retirement'  
when t1.social_code = '09' then 'Retirement'  
when t1.social_code = '10' then ''  
when t1.social_code = '11' then 'Retirement'  
when t1.social_code = '12' then 'Retirement'  
when t1.social_code = '13' then 'Retirement'  
when t1.social_code = '14' then 'Misc'  
when t1.social_code = '15' then 'Trust'  
when t1.social_code = '16' then 'Individual/Joint/Savings'  
when t1.social_code = '17' then 'Retirement'  
when t1.social_code = '18' then 'Broker/Dealer'  
when t1.social_code = '19' then 'Retirement'  
when t1.social_code = '20' then 'Retirement'  
when t1.social_code = '21' then 'Non-Profit'  
when t1.social_code = '22' then 'Retirement'  
when t1.social_code = '23' then 'Retirement'  
when t1.social_code = '24' then 'Retirement'  
when t1.social_code = '25' then 'Retirement'  
when t1.social_code = '26' then 'Misc'  
when t1.social_code = '27' then 'Misc'  
when t1.social_code = '28' then 'Retirement'  
when t1.social_code = '29' then 'Retirement'  
when t1.social_code = '30' then 'Retirement'  
when t1.social_code = '31' then 'Retirement'  
when t1.social_code = '32' then 'Retirement'  
when t1.social_code = '33' then 'Retirement'  
when t1.social_code = '34' then 'Retirement'  
when t1.social_code = '35' then 'Retirement'  
when t1.social_code = '36' then 'Retirement'  
when t1.social_code = '37' then 'Individual/Joint/Savings'  
when t1.social_code = '38' then 'Individual/Joint/Savings'  
when t1.social_code = '39' then 'Misc'  
when t1.social_code = '40' then 'Misc'  
when t1.social_code = '41' then 'Misc'  
when t1.social_code = '42' then 'Trust'  
when t1.social_code = '43' then 'Retirement'  
when t1.social_code = '44' then 'Retirement'  
when t1.social_code = '45' then 'Retirement'  
when t1.social_code = '46' then 'Retirement'  
when t1.social_code = '47' then 'Individual/Joint/Savings'  
when t1.social_code = '48' then 'Individual/Joint/Savings'  
when t1.social_code = '49' then 'Retirement'  
when t1.social_code = '50' then 'Retirement'  
when t1.social_code = '51' then 'Retirement'  
when t1.social_code = '52' then 'Individual/Joint/Savings'  
when t1.social_code = '53' then 'Retirement'  
when t1.social_code = '54' then 'Retirement'  
when t1.social_code = '55' then 'Misc'  
when t1.social_code = '56' then 'Misc'  
when t1.social_code = '57' then 'Retirement'  
when t1.social_code = '58' then 'Retirement'  
when t1.social_code = '59' then 'Retirement'  
when t1.social_code = '60' then 'Retirement'  
when t1.social_code = '61' then 'Misc'  
when t1.social_code = '62' then 'Misc'  
when t1.social_code = '63' then 'Corporations'  
when t1.social_code = '64' then 'Retirement'  
when t1.social_code = '65' then 'Individual/Joint/Savings'  
when t1.social_code = '66' then 'Individual/Joint/Savings'  
when t1.social_code = '67' then 'Misc'  
when t1.social_code = '68' then 'Misc'  
when t1.social_code = '69' then 'Retirement'  
when t1.social_code = '70' then 'Retirement'  
when t1.social_code = '71' then 'Retirement'  
when t1.social_code = '72' then 'Retirement'  
when t1.social_code = '73' then 'Retirement'  
when t1.social_code = '74' then 'Misc'  
when t1.social_code = '75' then 'Misc'  
when t1.social_code = '76' then 'Retirement'  
when t1.social_code = '77' then 'Retirement'  
when t1.social_code = '78' then 'Retirement'  
when t1.social_code = '79' then 'Retirement'  
when t1.social_code = '80' then 'Retirement'  
when t1.social_code = '81' then 'Individual/Joint/Savings'  
when t1.social_code = '82' then 'Retirement'  
when t1.social_code = '83' then 'Misc'  
when t1.social_code = '84' then 'Individual/Joint/Savings'  
when t1.social_code = '85' then 'Misc'  
when t1.social_code = '86' then 'Retirement'  
when t1.social_code = '87' then 'Retirement'  
when t1.social_code = '88' then 'Retirement'  
when t1.social_code = '89' then 'Retirement'  
when t1.social_code = '90' then 'Retirement'  
when t1.social_code = '91' then 'Retirement'  
when t1.social_code = '92' then 'Retirement'  
when t1.social_code = '93' then 'Retirement'  
when t1.social_code = '94' then 'Misc'  
when t1.social_code = '95' then 'Misc'  
when t1.social_code = '96' then 'Misc'  
when t1.social_code = '97' then 'Misc'  
when t1.social_code = '98' then 'Misc'  
when t1.social_code = '99' then 'Misc'  
when t1.social_code = '99' then 'Misc'  
else t1.social_code
end as social_code,
coalesce(sum(sales::float),0)::text as sales,
coalesce(sum(redemptions::float),0)::text as redemptions,
coalesce(sum(net_flow::float),0)::text as net_flow,
coalesce(sum(income_reinvest::float),0)::text as income_reinvest
from
jameslondal.ff_social_code_flows_sector t1
group by
fundfamilyname,
trade_date,
case 
when t1.social_code = '01' then 'Individual/Joint/Savings'  
when t1.social_code = '02' then 'Individual/Joint/Savings'  
when t1.social_code = '03' then 'Individual/Joint/Savings'  
when t1.social_code = '04' then 'Corporations'  
when t1.social_code = '05' then 'Misc'  
when t1.social_code = '06' then 'Broker/Dealer'  
when t1.social_code = '07' then 'Bank/Trust'  
when t1.social_code = '08' then 'Retirement'  
when t1.social_code = '09' then 'Retirement'  
when t1.social_code = '10' then ''  
when t1.social_code = '11' then 'Retirement'  
when t1.social_code = '12' then 'Retirement'  
when t1.social_code = '13' then 'Retirement'  
when t1.social_code = '14' then 'Misc'  
when t1.social_code = '15' then 'Trust'  
when t1.social_code = '16' then 'Individual/Joint/Savings'  
when t1.social_code = '17' then 'Retirement'  
when t1.social_code = '18' then 'Broker/Dealer'  
when t1.social_code = '19' then 'Retirement'  
when t1.social_code = '20' then 'Retirement'  
when t1.social_code = '21' then 'Non-Profit'  
when t1.social_code = '22' then 'Retirement'  
when t1.social_code = '23' then 'Retirement'  
when t1.social_code = '24' then 'Retirement'  
when t1.social_code = '25' then 'Retirement'  
when t1.social_code = '26' then 'Misc'  
when t1.social_code = '27' then 'Misc'  
when t1.social_code = '28' then 'Retirement'  
when t1.social_code = '29' then 'Retirement'  
when t1.social_code = '30' then 'Retirement'  
when t1.social_code = '31' then 'Retirement'  
when t1.social_code = '32' then 'Retirement'  
when t1.social_code = '33' then 'Retirement'  
when t1.social_code = '34' then 'Retirement'  
when t1.social_code = '35' then 'Retirement'  
when t1.social_code = '36' then 'Retirement'  
when t1.social_code = '37' then 'Individual/Joint/Savings'  
when t1.social_code = '38' then 'Individual/Joint/Savings'  
when t1.social_code = '39' then 'Misc'  
when t1.social_code = '40' then 'Misc'  
when t1.social_code = '41' then 'Misc'  
when t1.social_code = '42' then 'Trust'  
when t1.social_code = '43' then 'Retirement'  
when t1.social_code = '44' then 'Retirement'  
when t1.social_code = '45' then 'Retirement'  
when t1.social_code = '46' then 'Retirement'  
when t1.social_code = '47' then 'Individual/Joint/Savings'  
when t1.social_code = '48' then 'Individual/Joint/Savings'  
when t1.social_code = '49' then 'Retirement'  
when t1.social_code = '50' then 'Retirement'  
when t1.social_code = '51' then 'Retirement'  
when t1.social_code = '52' then 'Individual/Joint/Savings'  
when t1.social_code = '53' then 'Retirement'  
when t1.social_code = '54' then 'Retirement'  
when t1.social_code = '55' then 'Misc'  
when t1.social_code = '56' then 'Misc'  
when t1.social_code = '57' then 'Retirement'  
when t1.social_code = '58' then 'Retirement'  
when t1.social_code = '59' then 'Retirement'  
when t1.social_code = '60' then 'Retirement'  
when t1.social_code = '61' then 'Misc'  
when t1.social_code = '62' then 'Misc'  
when t1.social_code = '63' then 'Corporations'  
when t1.social_code = '64' then 'Retirement'  
when t1.social_code = '65' then 'Individual/Joint/Savings'  
when t1.social_code = '66' then 'Individual/Joint/Savings'  
when t1.social_code = '67' then 'Misc'  
when t1.social_code = '68' then 'Misc'  
when t1.social_code = '69' then 'Retirement'  
when t1.social_code = '70' then 'Retirement'  
when t1.social_code = '71' then 'Retirement'  
when t1.social_code = '72' then 'Retirement'  
when t1.social_code = '73' then 'Retirement'  
when t1.social_code = '74' then 'Misc'  
when t1.social_code = '75' then 'Misc'  
when t1.social_code = '76' then 'Retirement'  
when t1.social_code = '77' then 'Retirement'  
when t1.social_code = '78' then 'Retirement'  
when t1.social_code = '79' then 'Retirement'  
when t1.social_code = '80' then 'Retirement'  
when t1.social_code = '81' then 'Individual/Joint/Savings'  
when t1.social_code = '82' then 'Retirement'  
when t1.social_code = '83' then 'Misc'  
when t1.social_code = '84' then 'Individual/Joint/Savings'  
when t1.social_code = '85' then 'Misc'  
when t1.social_code = '86' then 'Retirement'  
when t1.social_code = '87' then 'Retirement'  
when t1.social_code = '88' then 'Retirement'  
when t1.social_code = '89' then 'Retirement'  
when t1.social_code = '90' then 'Retirement'  
when t1.social_code = '91' then 'Retirement'  
when t1.social_code = '92' then 'Retirement'  
when t1.social_code = '93' then 'Retirement'  
when t1.social_code = '94' then 'Misc'  
when t1.social_code = '95' then 'Misc'  
when t1.social_code = '96' then 'Misc'  
when t1.social_code = '97' then 'Misc'  
when t1.social_code = '98' then 'Misc'  
when t1.social_code = '99' then 'Misc'  
when t1.social_code = '99' then 'Misc'  
else t1.social_code
end 
order by
fundfamilyname,
case 
when t1.social_code = '01' then 'Individual/Joint/Savings'  
when t1.social_code = '02' then 'Individual/Joint/Savings'  
when t1.social_code = '03' then 'Individual/Joint/Savings'  
when t1.social_code = '04' then 'Corporations'  
when t1.social_code = '05' then 'Misc'  
when t1.social_code = '06' then 'Broker/Dealer'  
when t1.social_code = '07' then 'Bank/Trust'  
when t1.social_code = '08' then 'Retirement'  
when t1.social_code = '09' then 'Retirement'  
when t1.social_code = '10' then ''  
when t1.social_code = '11' then 'Retirement'  
when t1.social_code = '12' then 'Retirement'  
when t1.social_code = '13' then 'Retirement'  
when t1.social_code = '14' then 'Misc'  
when t1.social_code = '15' then 'Trust'  
when t1.social_code = '16' then 'Individual/Joint/Savings'  
when t1.social_code = '17' then 'Retirement'  
when t1.social_code = '18' then 'Broker/Dealer'  
when t1.social_code = '19' then 'Retirement'  
when t1.social_code = '20' then 'Retirement'  
when t1.social_code = '21' then 'Non-Profit'  
when t1.social_code = '22' then 'Retirement'  
when t1.social_code = '23' then 'Retirement'  
when t1.social_code = '24' then 'Retirement'  
when t1.social_code = '25' then 'Retirement'  
when t1.social_code = '26' then 'Misc'  
when t1.social_code = '27' then 'Misc'  
when t1.social_code = '28' then 'Retirement'  
when t1.social_code = '29' then 'Retirement'  
when t1.social_code = '30' then 'Retirement'  
when t1.social_code = '31' then 'Retirement'  
when t1.social_code = '32' then 'Retirement'  
when t1.social_code = '33' then 'Retirement'  
when t1.social_code = '34' then 'Retirement'  
when t1.social_code = '35' then 'Retirement'  
when t1.social_code = '36' then 'Retirement'  
when t1.social_code = '37' then 'Individual/Joint/Savings'  
when t1.social_code = '38' then 'Individual/Joint/Savings'  
when t1.social_code = '39' then 'Misc'  
when t1.social_code = '40' then 'Misc'  
when t1.social_code = '41' then 'Misc'  
when t1.social_code = '42' then 'Trust'  
when t1.social_code = '43' then 'Retirement'  
when t1.social_code = '44' then 'Retirement'  
when t1.social_code = '45' then 'Retirement'  
when t1.social_code = '46' then 'Retirement'  
when t1.social_code = '47' then 'Individual/Joint/Savings'  
when t1.social_code = '48' then 'Individual/Joint/Savings'  
when t1.social_code = '49' then 'Retirement'  
when t1.social_code = '50' then 'Retirement'  
when t1.social_code = '51' then 'Retirement'  
when t1.social_code = '52' then 'Individual/Joint/Savings'  
when t1.social_code = '53' then 'Retirement'  
when t1.social_code = '54' then 'Retirement'  
when t1.social_code = '55' then 'Misc'  
when t1.social_code = '56' then 'Misc'  
when t1.social_code = '57' then 'Retirement'  
when t1.social_code = '58' then 'Retirement'  
when t1.social_code = '59' then 'Retirement'  
when t1.social_code = '60' then 'Retirement'  
when t1.social_code = '61' then 'Misc'  
when t1.social_code = '62' then 'Misc'  
when t1.social_code = '63' then 'Corporations'  
when t1.social_code = '64' then 'Retirement'  
when t1.social_code = '65' then 'Individual/Joint/Savings'  
when t1.social_code = '66' then 'Individual/Joint/Savings'  
when t1.social_code = '67' then 'Misc'  
when t1.social_code = '68' then 'Misc'  
when t1.social_code = '69' then 'Retirement'  
when t1.social_code = '70' then 'Retirement'  
when t1.social_code = '71' then 'Retirement'  
when t1.social_code = '72' then 'Retirement'  
when t1.social_code = '73' then 'Retirement'  
when t1.social_code = '74' then 'Misc'  
when t1.social_code = '75' then 'Misc'  
when t1.social_code = '76' then 'Retirement'  
when t1.social_code = '77' then 'Retirement'  
when t1.social_code = '78' then 'Retirement'  
when t1.social_code = '79' then 'Retirement'  
when t1.social_code = '80' then 'Retirement'  
when t1.social_code = '81' then 'Individual/Joint/Savings'  
when t1.social_code = '82' then 'Retirement'  
when t1.social_code = '83' then 'Misc'  
when t1.social_code = '84' then 'Individual/Joint/Savings'  
when t1.social_code = '85' then 'Misc'  
when t1.social_code = '86' then 'Retirement'  
when t1.social_code = '87' then 'Retirement'  
when t1.social_code = '88' then 'Retirement'  
when t1.social_code = '89' then 'Retirement'  
when t1.social_code = '90' then 'Retirement'  
when t1.social_code = '91' then 'Retirement'  
when t1.social_code = '92' then 'Retirement'  
when t1.social_code = '93' then 'Retirement'  
when t1.social_code = '94' then 'Misc'  
when t1.social_code = '95' then 'Misc'  
when t1.social_code = '96' then 'Misc'  
when t1.social_code = '97' then 'Misc'  
when t1.social_code = '98' then 'Misc'  
when t1.social_code = '99' then 'Misc'  
when t1.social_code = '99' then 'Misc'  
else t1.social_code
end 
,trade_date
);
