﻿select
t2.sector,
avg(feeadj_return_y1) as feeadj_return,
sum(sales) as sales,
sum(redemptions) as redemptions,
sum(net_flow) as net_flow,
sum(income_reinvest) as income_reinvest,
avg(case when t1.fund_family = 'Legg Mason' then feeadj_return_y1 end )  as mf_feeadj_return,
sum(case when t1.fund_family = 'Legg Mason' then sales end) as mf_sales,
sum(case when t1.fund_family = 'Legg Mason' then redemptions end) as mf_redemptions,
sum(case when t1.fund_family = 'Legg Mason' then net_flow end) as mf_net_flow,
sum(case when t1.fund_family = 'Legg Mason' then income_reinvest end) as mf_income_reinvest
from
state_flows t1 join
morningstar t2 on (t1.ticker_symbol_id = t2.ticker_symbol_id)
where
trade_date between '2015-03-01' and '2015-06-01' and
t1.sector in (
                select
                distinct 
                sector
                from
                state_flows
                where
                fund_family = 'Legg Mason' and trade_date between '2015-03-01' and '2015-06-01'
                group by
                sector,
                state,
                trade_date
                having 
                sum(sales) <> 0
                    and
                sum(redemptions) <> 0
                    and 
                sum(income_reinvest) <> 0
)
group by
t2.sector



select
sector,
trim(state) as state,
trade_date::date as trade_date,
sum(sales) as sales,
sum(redemptions) as redemptions,
sum(net_flow) as net_flow,
sum(income_reinvest) as income_reinvest
from
state_flows
where
trade_date between '2015-03-01' and '2015-06-01' and
fund_family <> 'Legg Mason' and 
sector in (
                select
                distinct 
                sector
                from
                state_flows
                where
                fund_family = 'Legg Mason' and trade_date between '2015-03-01' and '2015-06-01'
                group by
                sector,
                state,
                trade_date
                having 
                sum(sales) <> 0
                    and
                sum(redemptions) <> 0
                    and 
                sum(income_reinvest) <> 0
)
group by
sector,
state,
trade_date
having 
sum(sales) <> 0
    and
sum(redemptions) <> 0
    and 
sum(income_reinvest) <> 0



select
            t2.sector,
            avg(feeadj_return_y1) as feeadj_return,
            sum(sales) as sales,
            sum(redemptions) as redemptions,
            sum(net_flow) as net_flow,
            sum(income_reinvest) as income_reinvest,
            avg(case when t1.fund_family = 'Legg Mason' then feeadj_return_y1 end )  as mf_feeadj_return,
            sum(case when t1.fund_family = 'Legg Mason' then sales end) as mf_sales,
            sum(case when t1.fund_family = 'Legg Mason' then redemptions end) as mf_redemptions,
            sum(case when t1.fund_family = 'Legg Mason' then net_flow end) as mf_net_flow,
            sum(case when t1.fund_family = 'Legg Mason' then income_reinvest end) as mf_income_reinvest
            from
            state_flows t1 join
            morningstar t2 on (t1.ticker_symbol_id = t2.ticker_symbol_id)
            where
            trade_date between '2015-03-01' and '2015-06-01' and
            t1.sector in (
                            select
                            distinct 
                            sector
                            from
                            state_flows
                            where
                            fund_family = 'Legg Mason' and trade_date between '2015-03-01' and '2015-06-01'
                            group by
                            sector,
                            state,
                            trade_date
                            having 
                            sum(sales) <> 0
                                and
                            sum(redemptions) <> 0
                                and 
                            sum(income_reinvest) <> 0
            )
            group by
            t2.sector


select
t2.fund_name,
t2.sector,
avg(feeadj_return_y1) as feeadj_return,
sum(sales) as sales,
sum(redemptions) as redemptions,
sum(net_flow) as net_flow,
sum(income_reinvest) as income_reinvest,
avg(case when t1.fund_family = 'Legg Mason' then feeadj_return_y1 end )  as mf_feeadj_return,
sum(case when t1.fund_family = 'Legg Mason' then sales end) as mf_sales,
sum(case when t1.fund_family = 'Legg Mason' then redemptions end) as mf_redemptions,
sum(case when t1.fund_family = 'Legg Mason' then net_flow end) as mf_net_flow,
sum(case when t1.fund_family = 'Legg Mason' then income_reinvest end) as mf_income_reinvest
from
state_flows t1 join
morningstar t2 on (t1.ticker_symbol_id = t2.ticker_symbol_id)
where
trade_date between '2015-03-01' and '2015-06-01' and
t1.sector in (
                select
                distinct 
                sector
                from
                state_flows
                where
                fund_family = 'Legg Mason' and trade_date between '2015-03-01' and '2015-06-01'
                group by
                sector,
                state,
                trade_date
                having 
                sum(sales) <> 0
                    and
                sum(redemptions) <> 0
                    and 
                sum(income_reinvest) <> 0
)
group by
t2.fund_name,
t2.sector


select
        t2.sector,
        avg(feeadj_return_y1)::text as feeadj_return,
        sum(net_flow)::text as net_flow,
        coalesce(avg(case when t1.fund_family = 'Legg Mason' then feeadj_return_y1 end ),0)::text  as LeggMason_feeadj_return,
        sum(case when t1.fund_family = 'Legg Mason' then net_flow end)::text as LeggMason_net_flow
        from
        state_flows t1 join
        morningstar t2 on (t1.ticker_symbol_id = t2.ticker_symbol_id)
        where
        trade_date = '2015-06-01' and
        t1.sector in (
                        select
                        distinct 
                        sector
                        from
                        state_flows
                        where
                        fund_family = 'Legg Mason' and trade_date between '2015-03-01' and '2015-06-01'
                        group by
                        sector,
                        state,
                        trade_date
                        having 
                        sum(sales) <> 0
                            and
                        sum(redemptions) <> 0
                            and 
                        sum(income_reinvest) <> 0
        )
        group by
        t2.sector
        having coalesce(avg(case when t1.fund_family = 'Legg Mason' then feeadj_return_y1 end ),0) <> 0

 select
 fund_family,
 sum(sales)
 from
 funds_flows
 group by fund_family
 order by sum(sales) desc
 